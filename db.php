<?php
    //Library untuk simpan, baca, update, delete data dari file .json    
    //author: Franklin Collin Tamboto | tamboto2000@gmail.com

    class DBDriver{
        private $db_file;

        public function __construct($db_name) {
            $db_access = file_get_contents('../storage/json_db/'.(string)$db_name.'.json');        

            if(mb_strlen($db_access) == 0) {
                fopen('../storage/json_db/'.(string)$db_name.'.json', '[]');

                $this->db_file = $db_name;
            }

            $this->db_file = $db_name;
        }            
        
        public function checkExisting($column_name, $value) {
            $db_file = file_get_contents('../storage/json_db/'.(string)$this->db_file.'.json');
            $db_f_decode = json_decode($db_file);

            foreach($db_f_decode as $data) {
                if((string)$data->$column_name == (string)$value) {
                    return true;

                    break;
                }                
            }

            return false;
        }

        public function getDataWhere($column_name, $value, $return_val) {
            $db_file = file_get_contents('../storage/json_db/'.(string)$this->db_file.'.json');
            $db_f_decode = json_decode($db_file);

            foreach($db_f_decode as $data) {
                if($data->$column_name == (string)$value) {
                    return $data->$return_val;

                    break;
                }
            }

            return false;
        }

        public function updateDataWhere($column_name, $value, $column_insert, $insert_val) {
            $db_file = file_get_contents('../storage/json_db/'.(string)$this->db_file.'.json');
            $db_f_decode = json_decode($db_file, true);

            foreach($db_f_decode as $key => $data) {
                if($data[(string)$column_name] == (string)$value) {                    
                    $data[(string)$column_insert] = $insert_val;
                    $data_array[] = $data;

                    file_put_contents('../storage/json_db/'.(string)$this->db_file.'.json', json_encode($data_array));

                    return true;

                    break;
                }
            }

            return false;
        }

        public function insertData($array_data) {
            $json_db = file_get_contents('../storage/json_db/'.(string)$this->db_file.'.json');
            $decode_db = json_decode($json_db, true);

            $decode_db[] = $array_data;

            $put_operation = file_put_contents('../storage/json_db/'.(string)$this->db_file.'.json', json_encode($decode_db));

            if($put_operation != false) {
                return true;
            }

            return false;
        }
    }
?>