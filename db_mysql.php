<?php    
    //MySqlHelper for less complex MySql database operation
    //Author: Franklin Collin Tamboto | tamboto2000@gmail.com
    /*PS: 
        you might be found this code is like S P H A G E T :v
    */

    class MySqlHelper {
        private $host;
        private $dbname;
        private $username;
        private $password;
        private $db_build;        

        public function setConfig($data_array) {
            $this->host = $data_array['host'];
            $this->dbname = $data_array['dbname'];
            $this->username = $data_array['username'];
            $this->password = $data_array['password'];

            return $this;
        }

        public function buildConnection(){
            try {
                $_DB = new PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->username, $this->password);    
                $_DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                
                $this->db_build = $_DB;
                return $this;
            } 
            catch (PDOException $exception){
                die("Connection error: " . $exception->getMessage());

                return false;
            }    
        }

        //Query operation

        private $query;
        private $query_string;
        private $result;
        private $prepare;
        private $bindParam;
        private $bindParamVal;

        public function selectFrom($column, $table) {
            $this->query['selected_col'] = $column;
            $this->query['table'] = $table;

            $this->query_string = 'select '.$column.' from '.$table;
            return $this;
        }

        public function where($column, $operator, $value) {                        
            $this->query_string .= ' where '.$column.' '.$operator.' '.$value;

            return $this;
        }    
        
        public function whereMultiple($data_array) {
            $this->query_string .= ' where ';

            foreach($data_array as $num => $data) {
                $this->bindParamVal[] = $data;                
            }

            foreach($this->bindParamVal as $num => $data) {
                if($num + 1 === count($this->bindParamVal)) {                    
                    $this->query_string .= $data[0].' '.$data[1].' \''.$data[2].'\';';
                }
                else {                    
                    $this->query_string .= $data[0].' '.$data[1].' \''.$data[2].'\'';
                    $this->query_string .= ' AND ';
                }
            }
                        
            return $this;
        }

        public function get() {
            $prepare_it = $this->db_build->prepare($this->query_string);
            $prepare_it->execute();
            
            return $prepare_it->fetch();
        }

        public function getAll() {
            $prepare_it = $this->db_build->prepare($this->query_string);
            $prepare_it->execute();
            
            return $prepare_it->fetchAll();
        }
        
        public function insertInto($table, $values) {            
            $this->query_string = "insert into ".$table."(";

            $keys = array_keys($values);

            foreach($keys as $num => $key) {
                if($num + 1 === count($keys)) {
                    $this->query_string .= $key.") values(";

                    foreach($keys as $num => $value) {
                        if($num + 1 === count($keys)) {                    
                            $this->bindParam[] = ":".$value;
                            $this->bindParamVal[] = $values[$value];

                            $this->query_string .= ":".$value.")";
                            $this->prepare = $this->db_build->prepare($this->query_string);

                            foreach($this->bindParam as $num => $param) {
                                $this->prepare->bindParam($param, $this->bindParamVal[$num]);
                            }
                        }
                        else {
                            $this->bindParam[] = ":".$value;
                            $this->bindParamVal[] = $values[$value];

                            $this->query_string .= ":".$value.",";
                        }
                    }
                }
                else {
                    $this->query_string .= $key.",";
                }
            }

            $this->prepare->execute();              
        }

        //This function is still buggy :( 
        public function update($table, $data_array) {            
            $keys = array_keys($data_array);
            $this->query_string = 'update '.$table.' set ';            

            foreach($keys as $num => $key) {
                $this->bindParam[] = $key;
                $this->bindParamVal[] = $data_array[$key];

                if($num + 1 === count($keys)) {
                    foreach($this->bindParam as $num => $param) {
                        if($num + 1 === count($this->bindParam)) {
                            $this->query_string .= $param.' = :'.$param.';';
                            $this->prepare = $this->db_build->prepare($this->query_string);

                            foreach($this->bindParam as $num => $val) {
                                $this->prepare->bindParam(':'.$val, $data_array[$val]);
                            }                            
                        }
                        else {
                            $this->query_string .= $param.' = :'.$param.' AND ';
                        }
                    }
                }
            }
            
            return $this;
        }

        //Just in case if we need to executing things manually
        public function execute() {
            try {
                $this->prepare->execute();

                return true;
            }
            catch(PDOException $e) {
                echo $e;

                return false;
            }
        }

        //(end) Query operation
    }
?>