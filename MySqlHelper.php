<?php    
    //MySqlHelper for less complex MySql database operation
    //Author: Franklin Collin Tamboto | tamboto2000@gmail.com
    /*PS: 
        you might be found this code is like S P H A G E T :v
    */

class MySqlHelper {
    private $host;
    private $dbname;
    private $username;
    private $password;
    private $db_build;

    //Build the connections

    public function setConfig($data_array) {
        $this->host = $data_array['host'];
        $this->dbname = $data_array['dbname'];
        $this->username = $data_array['username'];
        $this->password = $data_array['password'];

        return $this;
    }

    public function buildConnection(){
        try {
            $_DB = new PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->username, $this->password, [PDO::ATTR_TIMEOUT => 1500]);
            $_DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $this->db_build = $_DB;
            return $this;
        } 
        catch (PDOException $exception){
            die("Connection error: " . $exception->getMessage());

            return false;
        }    
    }

    //Query operation

    private $query;
    private $query_string;
    private $result;
    private $prepare;
    private $bindParam;
    private $bindParamVal;
    private $whereStatement;
    private $updateStatement;
    private $table;
    private $select;
    private $preparedBinding;

    public function table($table) {
        $this->table = $table;

        return $this;
    }

    public function select($field) {                
        $this->select = $field;

        return $this;
    }

    public function where($column = null, $operator = null, $value = null) {                        
        if(gettype($column) === "array") {
            $this->whereMultiple($column);

            return $this;
        }

        $this->bindParam[] = ':'.$column;
        $this->bindParamVal[] = $value;
        $this->whereStatement = 'where '.$column.' '.$operator.' '.':'.$column;    

        return $this;
    }    
        
    public function whereMultiple($data_array) {
        $this->whereStatement .= 'where ';
        $where;

        foreach($data_array as $num => $data) {
            $where[] = $data;                
        }

        foreach($where as $num => $data) {
            if($num + 1 === count($where)) {                    
                $this->whereStatement .= $data[0].' '.$data[1].' '.':'.$data[0];
            }
            else {                    
                $this->whereStatement .= $data[0].' '.$data[1].' '.':'.$data[0];
                $this->whereStatement .= ' AND ';                    
            }
            
            $this->bindParam[] = ':'.$data[0];
            $this->bindParamVal[] = $data[2];
        }
                    
        return $this;
    }

    public function get() {            
        if(!empty($this->select)) {
            $this->query_string = "select ".$this->select." from ".$this->table." ";

            if(!empty($this->whereStatement)) {
                $this->query_string .= $this->whereStatement;
            }
        }     
        else {
            $this->query_string = "select * from ".$this->table." ";

            if(!empty($this->whereStatement)) {
                $this->query_string .= $this->whereStatement;
            }
        }

        $this->prepareQuery();
        $this->execQuery();

        return $this->prepare->fetch();            
    }

    public function getAll() {
        if(!empty($this->select)) {
            $this->query_string = "select ".$this->select." from ".$this->table." ";

            if(!empty($this->whereStatement)) {
                $this->query_string .= $this->whereStatement;
            }
        }     
        else {
            $this->query_string = "select * from ".$this->table." ";

            if(!empty($this->whereStatement)) {
                $this->query_string .= $this->whereStatement;
            }
        }    
        
        $this->prepareQuery();        
        $this->execQuery();

        return $this->prepare->fetchAll();            
    }        

    public function insert($data_array) {
        $array_keys = array_keys($data_array);
        
        $this->query_string = "insert into ".$this->table." (";        

        foreach($array_keys as $num => $data) {
            $this->bindParam[] = ':'.$data;
            $this->bindParamVal[] = $data_array[$data];

            if($num + 1 === count($array_keys)) {
                $this->query_string .= "`".$data."`) values(";                

                foreach($array_keys as $num => $data) {
                    if($num + 1 === count($array_keys)) {
                        $this->query_string .= ":".$data.")";
                    }
                    else {
                        $this->query_string .= ":".$data.", ";
                    }
                }
            }
            else {
                $this->query_string .= "`".$data."`, ";
            }            
        }

        $this->prepareQuery();
        $this->execQuery();            
    }

    public function update($data) {
        $this->updateStatement = 'update '.$this->table.' set';
        $index = array_keys($data);            
        
        foreach($index as $num => $key) {
            if($num + 1 === count($index)) {
                $this->updateStatement .= ' '.$key.' = :'.$key;
            }
            else {
                $this->updateStatement .= ' '.$key.' = :'.$key;
                $this->updateStatement .= ', ';
            }

            $this->bindParam[] = ':'.$key;
            $this->bindParamVal[] = $data[$key];
        }

        if(!empty($this->whereStatement)) {
            $this->query_string = $this->updateStatement.' '.$this->whereStatement;
        }
        else {
            $this->query_string = $this->updateStatement;
        }

        $this->prepareQuery();
        $this->execQuery();
    }
    
    public function deleteAll($table) {
        $this->query_string = 'delete from '.$table;            
        
        return $this;
    }

    public function hasValue($field) {
        if(isset($this->select)) {
            $this->query_string = "select ".$this->select." from ".$this->table." ".$this->whereStatement;
        }     
        else {
            $this->query_string = "select * from ".$this->table." ".$this->whereStatement;
        }    

        $this->prepareQuery();
        $this->execQuery();

        $data = $this->prepare->fetch();

        if(!empty($data[$field])) {
            return true;
        }
        else {
            return false;
        }
    }

    public function exists() {
        if(isset($this->select)) {
            if(!empty($this->whereStatement)) {
                $this->query_string = "select ".$this->select." from ".$this->table." ".$this->whereStatement;
            }
            else {
                $this->query_string = "select ".$this->select." from ".$this->table;
            }
        }     
        else {            
            if(!empty($this->whereStatement)) {
                $this->query_string = "select * from ".$this->table." ".$this->whereStatement;
            }
            else {
                $this->query_string = "select * from ".$this->table;
            }
        }

        $this->prepareQuery();
        $this->execQuery();

        $data = $this->prepare->fetchAll();

        if(count($data) !== 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public function tableNames($limit = null) {
        if(!empty($limit)) {
            $this->query_string = "SELECT table_name FROM information_schema.tables where table_schema='".$this->dbname."' limit ".$limit;
        }
        $this->query_string = "SELECT table_name FROM information_schema.tables where table_schema='".$this->dbname."'";

        $this->prepareQuery();
        $this->execQuery();        
                
        return $this->prepare->fetchAll();
    }
    
    private function execQuery() {
        try {
            $this->prepare->execute();            

            $this->refresh();
        }
        catch(PDOException $e) {
            echo $e;
        }        
    }

    private function prepareQuery() {
        try {
            $this->prepare = $this->db_build->prepare($this->query_string);
        
            if(!empty($this->bindParam) && !empty($this->bindParamVal)) {
                foreach($this->bindParam as $key => $param) {
                    $this->prepare->bindParam($param, $this->bindParamVal[$key]);
                }
            }
        }
        catch(PDOException $e) {
            throw $e;
        }
    }
    
    //Set some variable empty
    private function refresh() {    
        unset($this->query_string);        
        unset($this->bindParam);
        unset($this->bindParamVal);
        unset($this->whereStatement);
    }

    //(end) Query operation

    //Get the constructed query
    public function params() {
        foreach($this->bindParam as $key => $param) {
            echo $param." => ".$this->bindParamVal[$key]."\n";
        }            
    }

    //Get the constructed binded parameter
    public function query() {
        echo $this->query_string."\n";
    }    
}

?>